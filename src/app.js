const Koa = require('koa')
const bodyParser = require('koa-bodyparser')
const session = require('koa-session')
const cors = require('@koa/cors')

const routers = require('./routers/index')
const sessionConfig = require('./config/session')
const apiAuth = require('./middlewares/apiAuth')
// 数据库
require('./utils/db')

const app = new Koa()
// session的key
app.keys = ['F3Mtj5RTPpErWaKspqjc']

// 配置ctx.body解析中间件
app.use(bodyParser())

// 配置跨域
app.use(
  cors({
    credentials: true
  })
)

// 配置session
app.use(session(sessionConfig, app))

app.use(apiAuth)

// 配置路由
app.use(routers.routes()).use(routers.allowedMethods())

app.listen(3000)
