const { USER_TYPE } = require('../utils/enum')
const RESPONSE_CODE = require('../config/responseCode')

const noSessionURL = ['/api/user/login']

const authMap = {
  [USER_TYPE.ADMIN]: [
    '/api/user/getMenu',
    '/api/user/logout',
    '/api/user/resetPwd',
    '/api/admin/courseList',
    '/api/admin/addCourse',
    '/api/admin/editCourse',
    '/api/admin/delCourse',
    '/api/admin/teacherList',
    '/api/admin/addTeacher',
    '/api/admin/editTeacher',
    '/api/admin/delTeacher',
    '/api/admin/studentList',
    '/api/admin/addStudent',
    '/api/admin/editStudent',
    '/api/admin/delStudent'
  ],
  [USER_TYPE.TEACHER]: [
    '/api/user/getMenu',
    '/api/user/logout',
    '/api/user/resetPwd',
    '/api/user/personalInfo',
    '/api/user/editPersonalInfo',
    '/api/teacher/courseList',
    '/api/teacher/recordList',
    '/api/teacher/editScore'
  ],
  [USER_TYPE.STUDENT]: [
    '/api/user/getMenu',
    '/api/user/logout',
    '/api/user/resetPwd',
    '/api/user/personalInfo',
    '/api/user/editPersonalInfo',
    '/api/student/courseList',
    '/api/student/changeCourseStatus',
    '/api/student/scoreList'
  ]
}

// 接口鉴权
module.exports = async (ctx, next) => {
  const url = ctx.request.path
  const session = ctx.cookies.get('koa:sess')
  const userType = ctx.session.userType

  if (noSessionURL.includes(url)) {
    // 不需要登录态的接口 直接next
    await next()
  } else {
    if (session) {
      // 有登录态 判断用户权限
      const authList = authMap[userType]

      if (authList.includes(url)) {
        await next()
      } else {
        // 无该接口权限
        ctx.body = {
          success: false,
          msg: '您没有该权限',
          code: RESPONSE_CODE.NO_AUTH
        }
      }
    } else {
      // 无登录态 返回登录态失效
      ctx.body = {
        success: false,
        msg: '登录态失效',
        code: RESPONSE_CODE.NO_SESSION
      }
    }
  }
}
