const {
  getCourseList,
  handleAddCourse,
  handleEditCourse,
  handleDelCourse,
  getTeacherList,
  handleAddTeacher,
  handleEditTeacher,
  handleDelTeacher,
  getStudentList,
  handleAddStudent,
  handleEditStudent,
  handleDelStudent
} = require('../services/AdminService')

module.exports = {
  // 获取课程列表
  async courseList(ctx) {
    const query = ctx.request.query
    const { page } = query
    const pageSize = 10

    const result = await getCourseList({ params: query })

    const range = result.slice((page - 1) * pageSize, page * pageSize)

    const list = range.map((item) => {
      const { f_id, f_name, f_teacher_name, f_intro, f_teacher_id } = item

      return {
        id: f_id,
        courseName: f_name,
        teacherName: f_teacher_name,
        courseIntro: f_intro,
        teacherId: f_teacher_id
      }
    })

    ctx.body = {
      success: true,
      data: {
        total: result.length,
        page: Number(page),
        list
      }
    }
  },

  // 新增课程
  async addCourse(ctx) {
    const formData = ctx.request.body

    const result = await handleAddCourse({ params: formData })

    if (result.success) {
      return (ctx.body = { success: true, msg: '新增成功' })
    }

    ctx.body = result
  },

  // 修改课程
  async editCourse(ctx) {
    const formData = ctx.request.body

    const result = await handleEditCourse({ params: formData })

    if (result.success) {
      return (ctx.body = { success: true, msg: '修改成功' })
    }

    ctx.body = result
  },

  // 删除课程
  async delCourse(ctx) {
    const formData = ctx.request.body

    const result = await handleDelCourse({ params: formData })

    if (result.success) {
      return (ctx.body = { success: true, msg: '删除成功' })
    }

    ctx.body = result
  },

  // 教师列表
  async teacherList(ctx) {
    const query = ctx.request.query
    const { page } = query
    const pageSize = 10

    const result = await getTeacherList({ params: query })

    const range = page
      ? result.slice((page - 1) * pageSize, page * pageSize)
      : result

    const list = range.map((item) => {
      const { f_id, f_name, f_sex, f_phone, f_rank, f_number } = item

      return {
        id: f_id,
        name: f_name,
        sex: f_sex,
        rank: f_rank,
        phone: f_phone,
        number: f_number
      }
    })

    ctx.body = {
      success: true,
      data: {
        total: result.length,
        page: Number(page),
        list
      }
    }
  },

  // 新增教师
  async addTeacher(ctx) {
    const formData = ctx.request.body

    const result = await handleAddTeacher({ params: formData })

    if (result.success) {
      return (ctx.body = { success: true, msg: '新增成功' })
    }

    ctx.body = result
  },

  // 修改教师
  async editTeacher(ctx) {
    const formData = ctx.request.body

    const result = await handleEditTeacher({ params: formData })

    if (result.success) {
      return (ctx.body = { success: true, msg: '修改成功' })
    }

    ctx.body = result
  },

  // 删除教师
  async delTeacher(ctx) {
    const formData = ctx.request.body

    const result = await handleDelTeacher({ params: formData })

    if (result.success) {
      return (ctx.body = { success: true, msg: '删除成功' })
    }

    ctx.body = result
  },

  // 学生列表
  async studentList(ctx) {
    const query = ctx.request.query
    const { page } = query
    const pageSize = 10

    const result = await getStudentList({ params: query })

    const range = result.slice((page - 1) * pageSize, page * pageSize)

    const list = range.map((item) => {
      const { f_id, f_name, f_sex, f_phone, f_number } = item

      return {
        id: f_id,
        name: f_name,
        sex: f_sex,
        phone: f_phone,
        number: f_number
      }
    })

    ctx.body = {
      success: true,
      data: {
        total: result.length,
        page: Number(page),
        list
      }
    }
  },

  // 新增学生
  async addStudent(ctx) {
    const formData = ctx.request.body

    const result = await handleAddStudent({ params: formData })

    if (result.success) {
      return (ctx.body = { success: true, msg: '新增成功' })
    }

    ctx.body = result
  },

  // 修改学生
  async editStudent(ctx) {
    const formData = ctx.request.body

    const result = await handleEditStudent({ params: formData })

    if (result.success) {
      return (ctx.body = { success: true, msg: '修改成功' })
    }

    ctx.body = result
  },

  // 删除学生
  async delStudent(ctx) {
    const formData = ctx.request.body

    const result = await handleDelStudent({ params: formData })

    if (result.success) {
      return (ctx.body = { success: true, msg: '删除成功' })
    }

    ctx.body = result
  }
}
