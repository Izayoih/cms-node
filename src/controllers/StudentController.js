const {
  getCourseList,
  handleChangeCourseStatus,
  getScoreList
} = require('../services/StudentService')

module.exports = {
  // 获取课程列表
  async courseList(ctx) {
    const id = ctx.session.id
    const query = ctx.request.query
    const { page } = query
    const pageSize = 10

    const result = await getCourseList({ id, params: query })

    const range = result.slice((page - 1) * pageSize, page * pageSize)

    const list = range.map((item) => {
      const { f_id, f_name, f_teacher_name, f_intro, f_is_selected } = item

      return {
        id: f_id,
        courseName: f_name,
        teacherName: f_teacher_name,
        courseIntro: f_intro,
        isSelected: f_is_selected
      }
    })

    ctx.body = {
      success: true,
      data: {
        total: result.length,
        page: Number(page),
        list
      }
    }
  },

  // 选课/取消选课
  async changeCourseStatus(ctx) {
    const id = ctx.session.id
    const formData = ctx.request.body

    const result = await handleChangeCourseStatus({ id, params: formData })

    ctx.body = result
  },

  // 获取成绩列表
  async scoreList(ctx) {
    const id = ctx.session.id
    const query = ctx.request.query
    const { page } = query
    const pageSize = 10

    const result = await getScoreList({ id, params: query })

    const range = result.slice((page - 1) * pageSize, page * pageSize)

    const list = range.map((item) => {
      const { f_id, f_course_name, f_teacher_name, f_grade, f_message } = item

      return {
        id: f_id,
        courseName: f_course_name,
        teacherName: f_teacher_name,
        score: f_grade,
        message: f_message
      }
    })

    ctx.body = {
      success: true,
      data: {
        total: result.length,
        page: Number(page),
        list
      }
    }
  }
}
