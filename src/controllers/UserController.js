const {
  handleLogin,
  handleResetPwd,
  getUserInfo,
  handleEditInfo
} = require('../services/UserService')
const { SEX } = require('../utils/enum')
const menuConfig = require('../config/menu')

module.exports = {
  // 登录
  async login(ctx) {
    const formData = ctx.request.body

    const { success, data, msg } = await handleLogin(formData)

    if (!success) {
      return (ctx.body = {
        success,
        msg
      })
    }

    ctx.session.id = data.id
    ctx.session.userType = data.userType

    ctx.body = {
      success,
      data: { name: data.name }
    }
  },

  // 登出
  async logout(ctx) {
    ctx.cookies.set('koa:sess', '', { maxAge: 0 })
    ctx.cookies.set('koa:sess.sig', '', { maxAge: 0 })

    ctx.body = {
      success: true,
      msg: ''
    }
  },

  // 修改密码
  async resetPwd(ctx) {
    const id = ctx.session.id
    const formData = ctx.request.body

    const result = await handleResetPwd({ id, params: formData })

    ctx.body = result
  },

  // 获取个人信息
  async personalInfo(ctx) {
    const id = ctx.session.id
    const userType = ctx.session.userType

    const result = await getUserInfo({ id, userType })

    if (result) {
      const responseData = {
        name: result.f_name,
        sex: SEX[result.f_sex],
        number: result.f_number,
        phone: result.f_phone,
        userType
      }
      if (result.f_rank) responseData.rank = result.f_rank

      return (ctx.body = {
        success: true,
        data: responseData
      })
    }

    ctx.body = {
      success: false,
      msg: '无此用户'
    }
  },

  // 修改个人信息
  async editInfo(ctx) {
    const id = ctx.session.id
    const userType = ctx.session.userType
    const formData = ctx.request.body

    const result = await handleEditInfo({ id, userType, params: formData })

    if (result) {
      return (ctx.body = {
        success: true,
        msg: '修改成功'
      })
    }

    ctx.body = {
      success: false,
      msg: '修改失败'
    }
  },

  // 获取目录
  async getMenu(ctx) {
    const userType = ctx.session.userType

    ctx.body = {
      success: true,
      data: menuConfig[userType]
    }
  }
}
