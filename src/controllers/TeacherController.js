const {
  getCourseList,
  getRecordList,
  handleEditScore
} = require('../services/TeacherService')

module.exports = {
  // 获取课程列表
  async courseList(ctx) {
    const id = ctx.session.id

    const result = await getCourseList({ id })

    const list = result.map((item) => {
      return {
        id: item.f_id,
        courseName: item.f_name
      }
    })

    ctx.body = {
      success: true,
      data: list
    }
  },

  // 获取记录列表
  async recordList(ctx) {
    const id = ctx.session.id
    const query = ctx.request.query
    const { page } = query
    const pageSize = 10

    const result = await getRecordList({ id, params: query })

    const range = result.slice((page - 1) * pageSize, page * pageSize)

    const list = range.map((item) => {
      const {
        f_id,
        f_course_id,
        f_grade,
        f_message,
        f_student_name,
        f_course_name
      } = item

      return {
        id: f_id,
        courseID: f_course_id,
        courseName: f_course_name,
        studentName: f_student_name,
        score: f_grade,
        message: f_message
      }
    })

    ctx.body = {
      success: true,
      data: {
        total: result.length,
        page: Number(page),
        list
      }
    }
  },

  // 提交评分
  async editScore(ctx) {
    const formData = ctx.request.body

    const result = await handleEditScore({ params: formData })

    if (result.success) {
      return (ctx.body = { success: true, msg: '评分成功' })
    }

    ctx.body = result
  }
}
