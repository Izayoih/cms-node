const router = require('koa-router')()
const UserController = require('../controllers/UserController')

const routers = router
  .post('/login', UserController.login)
  .post('/logout', UserController.logout)
  .post('/resetPwd', UserController.resetPwd)
  .get('/personalInfo', UserController.personalInfo)
  .post('/editPersonalInfo', UserController.editInfo)
  .get('/getMenu', UserController.getMenu)

module.exports = routers
