const router = require('koa-router')()
const StudentController = require('../controllers/StudentController')

const routers = router
  .get('/courseList', StudentController.courseList)
  .post('/changeCourseStatus', StudentController.changeCourseStatus)
  .get('/scoreList', StudentController.scoreList)

module.exports = routers
