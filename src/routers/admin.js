const router = require('koa-router')()
const AdminController = require('../controllers/AdminController')

const routers = router
  .get('/courseList', AdminController.courseList)
  .get('/teacherList', AdminController.teacherList)
  .get('/studentList', AdminController.studentList)
  .post('/addCourse', AdminController.addCourse)
  .post('/editCourse', AdminController.editCourse)
  .post('/delCourse', AdminController.delCourse)
  .post('/addTeacher', AdminController.addTeacher)
  .post('/editTeacher', AdminController.editTeacher)
  .post('/delTeacher', AdminController.delTeacher)
  .post('/addStudent', AdminController.addStudent)
  .post('/editStudent', AdminController.editStudent)
  .post('/delStudent', AdminController.delStudent)

module.exports = routers
