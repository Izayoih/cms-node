const router = require('koa-router')()
const TeacherController = require('../controllers/TeacherController')

const routers = router
  .get('/courseList', TeacherController.courseList)
  .get('/recordList', TeacherController.recordList)
  .post('/editScore', TeacherController.editScore)

module.exports = routers
