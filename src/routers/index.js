const router = require('koa-router')({ prefix: '/api' })

const admin = require('./admin')
const teacher = require('./teacher')
const student = require('./student')
const user = require('./user')

router.use('/admin', admin.routes(), admin.allowedMethods())
router.use('/teacher', teacher.routes(), teacher.allowedMethods())
router.use('/student', student.routes(), student.allowedMethods())
router.use('/user', user.routes(), user.allowedMethods())

module.exports = router
