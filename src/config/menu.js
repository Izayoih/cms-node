const { USER_TYPE } = require('../utils/enum')

const studentMenu = [
  {
    title: '学生',
    children: [
      {
        title: '选课管理',
        path: '/student/course'
      },
      {
        title: '成绩查询',
        path: '/student/score'
      }
    ]
  },
  {
    title: '个人中心',
    children: [
      {
        title: '个人信息',
        path: '/user/personalInfo'
      }
    ]
  }
]

const teacherMenu = [
  {
    title: '教师',
    children: [
      {
        title: '课程管理',
        path: '/teacher/course'
      }
    ]
  },
  {
    title: '个人中心',
    children: [
      {
        title: '个人信息',
        path: '/user/personalInfo'
      }
    ]
  }
]

const adminMenu = [
  {
    title: '管理员',
    children: [
      {
        title: '课程管理',
        path: '/admin/course'
      },
      {
        title: '学生管理',
        path: '/admin/student'
      },
      {
        title: '教师管理',
        path: '/admin/teacher'
      }
    ]
  }
]

module.exports = {
  [USER_TYPE.ADMIN]: adminMenu,
  [USER_TYPE.TEACHER]: teacherMenu,
  [USER_TYPE.STUDENT]: studentMenu
}
