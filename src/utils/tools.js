const isEmpty = (value) => {
  return value === undefined || value === ''
}

module.exports = {
  isEmpty
}
