const Path = require('path')
const { resolve } = require('path')
const sqlite3 = require('sqlite3').verbose()
const db = new sqlite3.Database(Path.resolve(__dirname, '../../db.sqlite3'))

const get = (...params) => {
  return new Promise((resolve, reject) => {
    db.get(...params, (error, row) => {
      if (error) {
        reject(error)
      } else {
        resolve(row)
      }
    })
  })
}

const all = (...params) => {
  return new Promise((resolve, reject) => {
    db.all(...params, (error, row) => {
      if (error) {
        reject(error)
      } else {
        resolve(row)
      }
    })
  })
}

const run = (...params) => {
  return new Promise((resolve, reject) => {
    db.run(...params, function (error, row) {
      if (error) {
        reject(error)
      } else {
        resolve({ row, _id: this.lastID })
      }
    })
  })
}

module.exports = {
  get,
  all,
  run
}
