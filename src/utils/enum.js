// 用户类型
const USER_TYPE = {
  ADMIN: 1,
  TEACHER: 2,
  STUDENT: 3
}

// 性别
const SEX_MALE = 1
const SEX_FEMALE = 2
const SEX = {
  [SEX_MALE]: '男',
  [SEX_FEMALE]: '女'
}

module.exports = {
  USER_TYPE,
  SEX
}
