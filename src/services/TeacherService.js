const DB = require('../utils/db')

async function getTeacherID({ id }) {
  const user = await DB.get(`
    SELECT f_profile_id AS teacherID
    FROM t_user
    WHERE f_id=${id}
  `)

  return user.teacherID
}

module.exports = {
  async getCourseList({ id }) {
    const teacherID = await getTeacherID({ id })

    const result = DB.all(`
      SELECT f_name, f_id
      FROM t_course AS c
      WHERE c.f_teacher_id=${teacherID} AND f_is_delete=0
    `)

    return result
  },

  async getRecordList({ id, params }) {
    const teacherID = await getTeacherID({ id })

    const { courseID, studentName } = params

    const where = [
      'c.f_is_delete = 0',
      'sc.f_is_delete = 0',
      `c.f_teacher_id=${teacherID}`
    ]
    if (studentName) {
      where.push(`f_student_name LIKE '%${studentName}%' `)
    }
    if (courseID) {
      where.push(`sc.f_course_id = ${courseID}`)
    }
    const whereString = `WHERE ${where.join(' AND ')}`

    const result = await DB.all(`
      SELECT sc.f_id, sc.f_course_id, sc.f_grade, sc.f_message, s.f_name AS f_student_name, c.f_name AS f_course_name
      FROM t_select_course AS sc
      INNER JOIN t_course AS c ON sc.f_course_id=c.f_id
      INNER JOIN t_student AS s ON sc.f_student_id=s.f_id
      ${whereString};
    `)

    return result
  },

  async handleEditScore({ params }) {
    const { score, message, id } = params

    if (!id) {
      return { success: false, msg: '缺少id' }
    }

    await DB.run(`
      UPDATE t_select_course AS sc
      SET f_grade=${score}, f_message='${message}'
      WHERE sc.f_id=${id}
    `)

    return { success: true }
  }
}
