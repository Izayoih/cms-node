const DB = require('../utils/db')
const { isEmpty } = require('../utils/tools')

module.exports = {
  async getCourseList({ id, params }) {
    const { isSelected, courseName, courseID } = params

    const where = ['c.f_is_delete = 0']
    if (!isEmpty(isSelected)) {
      where.push(`f_is_selected = ${isSelected}`)
    }
    if (courseName) {
      where.push(`c.f_name LIKE '%${courseName}%' `)
    }
    if (courseID) {
      where.push(`c.f_id = ${courseID}`)
    }
    const whereString = `WHERE ${where.join(' AND ')}`

    const result = await DB.all(`
      SELECT c.f_id, c.f_name, c.f_intro, t.f_name AS f_teacher_name,
      (
        CASE WHEN
          (
            SELECT COUNT(*)
            FROM t_select_course AS sc
            WHERE sc.f_student_id=${id} AND sc.f_course_id=c.f_id AND sc.f_is_delete=0
          ) = 1
        THEN 1
        ELSE 0
        END
      ) AS f_is_selected
      FROM t_course AS c
      INNER JOIN t_teacher AS t ON c.f_teacher_id=t.f_id
      ${whereString};
    `)

    return result
  },

  async handleChangeCourseStatus({ id, params }) {
    const { id: courseID, action } = params

    if (!courseID) {
      return { success: false, msg: '缺少课程ID' }
    }

    // 取消选课
    if (action === 0) {
      await DB.run(`
        UPDATE t_select_course AS sc
        SET f_is_delete = 1
        WHERE sc.f_student_id=${id} AND sc.f_course_id=${courseID};
      `)

      return { success: true, msg: '取消成功' }
    }

    if (action === 1) {
      // 查询是否有选课记录
      const target = await DB.get(`
        SELECT *
        FROM t_select_course AS sc
        WHERE sc.f_student_id=${id} AND sc.f_course_id=${courseID};
      `)

      if (target) {
        await DB.run(`
          UPDATE t_select_course AS sc
          SET f_is_delete = 0
          WHERE sc.f_student_id=${id} AND sc.f_course_id=${courseID};
        `)
      } else {
        await DB.run(`
          INSERT INTO t_select_course (f_student_id, f_course_id)
          VALUES (${id}, ${courseID})
        `)
      }

      return { success: true, msg: '选课成功' }
    }

    return { success: false, msg: '参数有误' }
  },

  async getScoreList({ id, params }) {
    const { courseName, courseID } = params

    const where = [
      'c.f_is_delete = 0',
      'sc.f_is_delete=0',
      `sc.f_student_id=${id}`
    ]
    if (courseName) {
      where.push(`c.f_name LIKE '%${courseName}%' `)
    }
    if (courseID) {
      where.push(`c.f_id = ${courseID}`)
    }
    const whereString = `WHERE ${where.join(' AND ')}`

    const result = await DB.all(`
      SELECT c.f_id, sc.f_grade, sc.f_message, c.f_name as f_course_name, t.f_name as f_teacher_name
      FROM t_select_course AS sc
      INNER JOIN t_course AS c ON sc.f_course_id = c.f_id
      INNER JOIN t_teacher AS t ON c.f_teacher_id = t.f_id
      ${whereString};
    `)

    return result
  }
}
