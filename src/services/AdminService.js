const DB = require('../utils/db')

module.exports = {
  async getCourseList({ params }) {
    const { courseName, courseID } = params

    const where = ['c.f_is_delete = 0']
    if (courseName) {
      where.push(`c.f_name LIKE '%${courseName}%'`)
    }
    if (courseID) {
      where.push(`c.f_id = ${courseID}`)
    }

    const whereString = `WHERE ${where.join(' AND ')}`

    const result = await DB.all(`
      SELECT c.f_id,c.f_name,c.f_teacher_id,c.f_intro,t.f_name as f_teacher_name
      FROM t_course as c
      INNER JOIN t_teacher as t
      ON t.f_id = c.f_teacher_id
      ${whereString};
    `)

    return result
  },

  async handleAddCourse({ params }) {
    const { courseName, teacherId, courseIntro = '' } = params

    if (!courseName) {
      return { success: false, msg: '缺少课程名' }
    }
    if (!teacherId) {
      return { success: false, msg: '缺少主讲教师' }
    }

    await DB.run(`
      INSERT INTO t_course (f_name, f_intro, f_teacher_id)
      VALUES ('${courseName}', '${courseIntro}', ${teacherId});
    `)

    return { success: true }
  },

  async handleEditCourse({ params }) {
    const { id, courseName, teacherId, courseIntro } = params

    if (!id) {
      return { success: false, msg: '缺少课程id' }
    }
    if (!courseName) {
      return { success: false, msg: '缺少课程名' }
    }
    if (!teacherId) {
      return { success: false, msg: '缺少主讲教师' }
    }

    await DB.run(`
      UPDATE t_course
      SET f_name='${courseName}', f_teacher_id=${teacherId}, f_intro='${courseIntro}'
      WHERE f_id = ${id};
    `)

    return { success: true }
  },

  async handleDelCourse({ params }) {
    const { id } = params

    if (!id) {
      return { success: false, msg: '缺少课程id' }
    }

    await DB.run(`
      UPDATE t_course
      SET f_is_delete=1
      WHERE f_id = ${id};
    `)

    return { success: true }
  },

  async getTeacherList({ params }) {
    const { name, number } = params

    const where = ['t.f_is_delete = 0']
    if (name) {
      where.push(`t.f_name LIKE '%${name}%'`)
    }
    if (number) {
      where.push(`t.f_number = '${number}'`)
    }
    const whereString = `WHERE ${where.join(' AND ')}`

    const result = await DB.all(`
      SELECT f_id,f_name,f_sex,f_phone,f_rank,f_number
      FROM t_teacher AS t
      ${whereString};
    `)

    return result
  },

  async handleAddTeacher({ params }) {
    const { number, name, sex, rank, phone } = params

    if (!number) {
      return { success: false, msg: '缺少工号' }
    }
    if (!name) {
      return { success: false, msg: '缺少姓名' }
    }
    if (!sex) {
      return { success: false, msg: '缺少性别' }
    }
    if (!rank) {
      return { success: false, msg: '缺少职称' }
    }
    if (!phone) {
      return { success: false, msg: '缺少联系电话' }
    }

    const target = await DB.get(`
      SELECT * FROM t_user
      WHERE f_username = '${number}'
    `)

    if (target) return { success: false, msg: '工号已存在' }

    // teacher表插数据
    const { _id: profileId } = await DB.run(`
      INSERT INTO t_teacher (f_name, f_sex, f_phone, f_rank, f_number)
      VALUES ('${name}', '${sex}', '${phone}', '${rank}', '${number}');
    `)

    // user表插数据
    const { _id: userId } = await DB.run(`
      INSERT INTO t_user (f_username, f_user_type, f_profile_id)
      VALUES ('${number}', ${2}, ${profileId});
    `)

    // 更新teacher表
    await DB.run(`
      UPDATE t_teacher
      SET f_user_id = ${userId}
      WHERE f_id = ${profileId};
    `)

    return { success: true }
  },

  async handleEditTeacher({ params }) {
    const { id, name, sex, rank, phone } = params

    if (!id) {
      return { success: false, msg: '缺少id' }
    }
    if (!name) {
      return { success: false, msg: '缺少姓名' }
    }
    if (!sex) {
      return { success: false, msg: '缺少性别' }
    }
    if (!rank) {
      return { success: false, msg: '缺少职称' }
    }
    if (!phone) {
      return { success: false, msg: '缺少联系电话' }
    }

    await DB.run(`
      UPDATE t_teacher
      SET f_name='${name}', f_sex=${sex}, f_rank='${rank}', f_phone='${phone}'
      WHERE f_id=${id};
    `)

    return { success: true }
  },

  async handleDelTeacher({ params }) {
    const { id } = params

    if (!id) {
      return { success: false, msg: '缺少id' }
    }

    const target = await DB.get(`
      SELECT f_user_id
      FROM t_teacher
      WHERE f_id = ${id};
    `)

    await DB.run(`
      UPDATE t_teacher
      SET f_is_delete=1
      WHERE f_id = ${id};
    `)

    await DB.run(`
      UPDATE t_user
      SET f_is_delete=1
      WHERE f_id = ${target.f_user_id}
    `)

    return { success: true }
  },

  async getStudentList({ params }) {
    const { name, number } = params

    const where = ['s.f_is_delete = 0']
    if (name) {
      where.push(`s.f_name LIKE '%${name}%'`)
    }
    if (number) {
      where.push(`s.f_number = '${number}'`)
    }
    const whereString = `WHERE ${where.join(' AND ')}`

    const result = await DB.all(`
      SELECT f_id,f_name,f_sex,f_phone,f_number
      FROM t_student AS s
      ${whereString};
    `)

    return result
  },

  async handleAddStudent({ params }) {
    const { number, name, sex, phone } = params

    if (!number) {
      return { success: false, msg: '缺少工号' }
    }
    if (!name) {
      return { success: false, msg: '缺少姓名' }
    }
    if (!sex) {
      return { success: false, msg: '缺少性别' }
    }
    if (!phone) {
      return { success: false, msg: '缺少联系电话' }
    }

    const target = await DB.get(`
      SELECT * FROM t_user
      WHERE f_username = '${number}'
    `)

    if (target) return { success: false, msg: '学号已存在' }

    // student表插数据
    const { _id: profileId } = await DB.run(`
      INSERT INTO t_student (f_name, f_sex, f_phone, f_number)
      VALUES ('${name}', '${sex}', '${phone}', '${number}');
    `)

    // user表插数据
    const { _id: userId } = await DB.run(`
      INSERT INTO t_user (f_username, f_user_type, f_profile_id)
      VALUES ('${number}', ${3}, ${profileId});
    `)

    // 更新student表
    await DB.run(`
      UPDATE t_student
      SET f_user_id = ${userId}
      WHERE f_id = ${profileId};
    `)

    return { success: true }
  },

  async handleEditStudent({ params }) {
    const { id, name, sex, phone } = params

    if (!id) {
      return { success: false, msg: '缺少id' }
    }
    if (!name) {
      return { success: false, msg: '缺少姓名' }
    }
    if (!sex) {
      return { success: false, msg: '缺少性别' }
    }
    if (!phone) {
      return { success: false, msg: '缺少联系电话' }
    }

    await DB.run(`
      UPDATE t_student
      SET f_name='${name}', f_sex=${sex}, f_phone='${phone}'
      WHERE f_id=${id};
    `)

    return { success: true }
  },

  async handleDelStudent({ params }) {
    const { id } = params

    if (!id) {
      return { success: false, msg: '缺少id' }
    }

    const target = await DB.get(`
      SELECT f_user_id
      FROM t_student
      WHERE f_id = ${id};
    `)

    await DB.run(`
      UPDATE t_student
      SET f_is_delete=1
      WHERE f_id = ${id};
    `)

    await DB.run(`
      UPDATE t_user
      SET f_is_delete=1
      WHERE f_id = ${target.f_user_id}
    `)

    return { success: true }
  }
}
