const DB = require('../utils/db')
const { generateMD5 } = require('../utils/md5')
const { USER_TYPE } = require('../utils/enum')

module.exports = {
  async handleLogin(params) {
    const { username, password } = params

    const result = await DB.get(
      `SELECT * FROM 
      (SELECT f_name,f_user_id FROM t_teacher
        UNION SELECT f_name,f_user_id FROM t_student
        UNION SELECT f_name,f_user_id FROM t_admin) AS a
      INNER JOIN t_user on t_user.f_id=a.f_user_id
      WHERE f_username = $username AND f_is_delete = ${0}`,
      {
        $username: username
      }
    )

    // 用户不存在
    if (!result) return { success: false, msg: '用户名错误' }

    const { f_password, f_name, f_id, f_user_type } = result
    // 密码未设置
    if (!f_password) {
      // 初始密码和工号相同
      if (username === password) {
        await DB.run(
          'UPDATE t_user SET f_password = $password WHERE f_username = $username',
          { $username: username, $password: generateMD5(password) }
        )
        return {
          success: true,
          data: { name: f_name, id: f_id, userType: f_user_type }
        }
      }

      return { success: false, msg: '密码错误' }
    }

    if (generateMD5(password) === f_password) {
      return {
        success: true,
        data: { name: f_name, id: f_id, userType: f_user_type }
      }
    }

    return { success: false, msg: '密码错误' }
  },

  async handleResetPwd({ id, params }) {
    const { password, newPassword } = params
    const result = await DB.get(
      'select f_password from t_user where f_id = $id',
      { $id: id }
    )

    const { f_password } = result
    // 未设置密码 或 现密码匹配成功
    if (f_password === null || f_password === generateMD5(password)) {
      await DB.run(
        'UPDATE t_user SET f_password = $password WHERE f_id = $id',
        { $id: id, $password: generateMD5(newPassword) }
      )
      return {
        success: true,
        msg: '修改成功'
      }
    }

    return {
      success: false,
      msg: '现密码错误'
    }
  },

  async getUserInfo({ id, userType }) {
    const commonFields = ['f_name', 'f_sex', 'f_number', 'f_phone']
    const map = {
      [USER_TYPE.TEACHER]: {
        tableName: 't_teacher',
        fields: [...commonFields, 'f_rank']
      },
      [USER_TYPE.STUDENT]: {
        tableName: 't_student',
        fields: [...commonFields]
      }
    }

    const type = map[userType]

    const result = await DB.get(
      `SELECT ${type.fields.join(',')} FROM ${
        type.tableName
      } WHERE f_user_id = $id`,
      { $id: id }
    )

    return result
  },

  async handleEditInfo({ id, userType, params }) {
    const map = {
      [USER_TYPE.TEACHER]: {
        tableName: 't_teacher'
      },
      [USER_TYPE.STUDENT]: {
        tableName: 't_student'
      }
    }

    const type = map[userType]
    const { phone } = params

    const result = await DB.run(
      `UPDATE ${type.tableName} SET f_phone = $phone WHERE f_id = $id`,
      {
        $phone: phone,
        $id: id
      }
    )

    return true
  }
}
